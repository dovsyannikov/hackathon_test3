package com.example.hackathon3.models;

import java.util.List;

public class RequestModel {
        List <Film> results;

        public static class Film{
                private Integer id;
                private String poster_path;
                private String title;

                public String getPoster_path() {
                        return poster_path;
                }

                public String getTitle() {
                        return title;
                }

                    public int getId() {
                        return id;
                    }

                public Film(Integer id, String poster_path, String title) {
                        this.id = id;
                        this.poster_path = poster_path;
                        this.title = title;
                }
        }

        public List<Film> getResults() {
                return results;
        }
}
