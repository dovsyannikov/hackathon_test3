package com.example.hackathon3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hackathon3.ApiService.ApiService;
import com.example.hackathon3.models.FilmInfo;
import com.example.hackathon3.models.RequestModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MovieInfoActivity extends AppCompatActivity {
    public static final String ID = "id";
    public static final String NAME = "name";
    int defaultValue = 0;

    @BindView(R.id.fi_imageView2)
    ImageView fi_imageView2;
    @BindView(R.id.fi_txtName)
    TextView fi_txtName;
    @BindView(R.id.fi_txtDescription)
    TextView fi_txtDescription;
    @BindView(R.id.fi_txtDirector)
    TextView fi_txtDirector;
    @BindView(R.id.fi_txtYear2)
    TextView fi_txtYear2;

    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);
        setTitle("Film Info");
        ButterKnife.bind(this);

        final Intent intent = getIntent();


        disposable = ApiService.getFilmById(intent.getStringExtra(ID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<FilmInfo>() {
                    @Override
                    public void accept(FilmInfo filmInfo) throws Exception {
                        Toast.makeText(MovieInfoActivity.this, "gooood", Toast.LENGTH_SHORT).show();
                        Picasso.get().load(Converter.convertImagePathToFullUrl(filmInfo.getBackdrop_path())).into(fi_imageView2);
                        fi_txtName.setText(String.format("NAME: %s", intent.getStringExtra(NAME)));
                        fi_txtDescription.setText(String.format("DESCRIPTION: %s", Converter.stringLimitation(filmInfo.getOverview())));
                        fi_txtDirector.setText(String.format("RELEASE DATE: %s", filmInfo.getRelease_date()));
                        fi_txtYear2.setText(String.format("RATE: %s", filmInfo.getVote_average()));
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MovieInfoActivity.this, "baaaad", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
