package com.example.hackathon3.Database;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Movie {
    @PrimaryKey
    private int id;
    private String poster_path;
    private String title;

    public void setUniqueIDofProduct(int uniqueIDofProduct) {
        this.uniqueIDofProduct = uniqueIDofProduct;
    }

    private int uniqueIDofProduct;

    public int getId() {
        return id;
    }

    public int getUniqueIDofProduct() {
        return uniqueIDofProduct;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Movie() {
    }

    @Ignore

    public Movie(int id, String poster_path, String title, int uniqueIDofProduct) {
        this.id = id;
        this.poster_path = poster_path;
        this.title = title;
        this.uniqueIDofProduct = uniqueIDofProduct;
    }
}
