package com.example.hackathon3.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Movie.class}, version = 2, exportSchema = false)
public abstract class MyDataBase extends RoomDatabase {
    public abstract MovieDAO getMovieDao();
}
