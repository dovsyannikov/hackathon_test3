package com.example.hackathon3.Database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class MovieDAO {

    @Insert
    public abstract void insertAll(List<Movie> movies);
    @Query("SELECT * FROM Movie")
    public abstract Flowable<List<Movie>> selectAll();

    @Query("DELETE FROM Movie")
    public abstract void removeAll();

    @Query("SELECT * FROM Movie WHERE title LIKE :value OR poster_path LIKE :value")
    public abstract Flowable<List<Movie>> customSearch(String value);
}
