package com.example.hackathon3;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Insert;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.hackathon3.Adapter.FilmAdapter;
import com.example.hackathon3.ApiService.ApiService;
import com.example.hackathon3.Database.Movie;
import com.example.hackathon3.Database.MyDataBase;
import com.example.hackathon3.models.RequestModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.filmView)
    RecyclerView filmView;
    @BindView(R.id.btnSearch)
    ImageButton btnSearch;
    @BindView(R.id.txtSearchBar)
    EditText txtSearchBar;

    static FilmAdapter filmAdapter;

    Disposable disposable;

    MyDataBase dataBase;

    CompositeDisposable disposables = new CompositeDisposable();

    List<Movie> movies = new ArrayList<>();

    public static final String ID = "id";
    public static final String NAME = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar);

        ButterKnife.bind(this);

        setTitle("Films");

        apiRequest();

        printCountries();

    }

    private void apiRequest() {
        disposable = ApiService.getPopularFilms()
                .map(new Function<RequestModel, Boolean>() {
                    @Override
                    public Boolean apply(RequestModel requestModel) throws Exception {
                        movies = Converter.convertModelToMovie(requestModel);
                        Log.d("qqqqq", movies.get(1).getPoster_path()+"");
                        dataBase.getMovieDao().removeAll();
                        dataBase.getMovieDao().insertAll(movies);
                        return true;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Boolean>() {
                               @Override
                               public void accept(Boolean aBoolean) throws Exception {
                                   /*do noting*/
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {
                                   Toast.makeText(MainActivity.this, "Smth went wrong", Toast.LENGTH_SHORT).show();
                               }
                           });
        dataBase = Room.databaseBuilder(this, MyDataBase.class, "database")
                .fallbackToDestructiveMigration()
                .build();
    }

    private void printCountries(){
        disposables.add(dataBase.getMovieDao().selectAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Movie>>() {
                    @Override
                    public void accept(List<Movie> movies) throws Exception {
                        List<RequestModel.Film> films = Converter.convertMovieToModel(movies);
                        Log.d("wwww", films.get(1).getPoster_path()+"");
                        filmAdapter = new FilmAdapter(MainActivity.this, films);
                        filmView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                        filmView.setAdapter(filmAdapter);
                        onItemClick();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, "Smth went wrong2", Toast.LENGTH_SHORT).show();
                    }
                }));
    }

    private void onItemClick() {
        filmAdapter.setListener(new FilmAdapter.OnItemTouchListener() {
            @Override
            public void onClick(RequestModel.Film film) {
                Intent intent = new Intent(MainActivity.this, MovieInfoActivity.class);
                intent.putExtra(ID, String.valueOf(film.getId()));
                intent.putExtra(NAME, film.getTitle());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }
    @OnTouch(R.id.btnSearch)
    public boolean onBtnSearchClick(){
        String text = Converter.convertStringToSearchableQuery(txtSearchBar.getText().toString());

        disposables.add(dataBase.getMovieDao().customSearch(text)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Movie>>() {
                    @Override
                    public void accept(List<Movie> movies) throws Exception {
                        List<RequestModel.Film> films = Converter.convertMovieToModel(movies);
                        Toast.makeText(MainActivity.this, films.size()+"", Toast.LENGTH_SHORT).show();
                        filmAdapter = new FilmAdapter(MainActivity.this, films);
                        filmView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                        filmView.setAdapter(filmAdapter);
                        onItemClick();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Log.d("eeee", "WRRRRRONG" + "");
                    }
                }));
        return true;
    }
}
