package com.example.hackathon3;

import com.example.hackathon3.Database.Movie;
import com.example.hackathon3.models.RequestModel;

import java.util.ArrayList;
import java.util.List;

public class Converter {
    public static String convertImagePathToFullUrl(String imagePath){
        return "https://image.tmdb.org/t/p/w500/" + imagePath;
    }
    public static List<Movie> convertModelToMovie(RequestModel requestModel){
        List<Movie> films = new ArrayList<>();
        int i = 1;
        for (RequestModel.Film result : requestModel.getResults()) {
            films.add(new Movie(i++, result.getPoster_path(), result.getTitle(), result.getId()));
        }
        return films;
    }
    public static List<RequestModel.Film> convertMovieToModel(List<Movie> movies){
        List<RequestModel.Film> films = new ArrayList<>();
        int i = 1;
        for (Movie movie : movies) {
            films.add(new RequestModel.Film(movie.getUniqueIDofProduct(), movie.getPoster_path(), movie.getTitle()));
        }
        return films;
    }
    public static String stringLimitation(String bigString){
        String cutString = new String();
        if (bigString.length() > 75){
            cutString = bigString.substring(0, 75);
        }
        return cutString + "...";
    }
    public static String convertStringToSearchableQuery (String query){
        return "%"+query+"%";
    }
}
