package com.example.hackathon3.ApiService;

import com.example.hackathon3.models.FilmInfo;
import com.example.hackathon3.models.RequestModel;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://api.themoviedb.org/3/movie/";
    private static PrivateApi privateApi;

    public static final String API_KEY = "3cbed7c3b4a0b792c1a5f9374762978c";

    public interface PrivateApi {

        //https://api.themoviedb.org/3/movie/popular?api_key=3cbed7c3b4a0b792c1a5f9374762978c&language=en-US&page=1

        //https://api.themoviedb.org/3/movie/{movie_id}/external_ids?api_key=<<api_key>>

        @GET("popular")
        Observable<RequestModel> getPopular(@Query("api_key") String api_key);

        @GET("{movie_id}")
        Observable<FilmInfo> getById(@Path("movie_id") String movieID, @Query("api_key") String api_key);


    }
    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);
    }

    public static Observable<RequestModel> getPopularFilms (){
        return privateApi.getPopular(API_KEY);
    }

    public static Observable<FilmInfo> getFilmById (String movieId){
        return privateApi.getById(movieId, API_KEY);
    }
}
